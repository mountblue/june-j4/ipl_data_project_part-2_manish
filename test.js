const expect = require("chai").expect;
const path = require("path");
const file = path.resolve("./app");
const operartion = require(file);

describe("Matches won by teams", function () {

    it("should not return undefined", async function () {
        expectedResult = undefined;
        let result = await operartion.getMatchPerYear("testdb", "matchestest");
        expect(result).deep.not.equals(expectedResult);
    })

    it("should not return null", async function () {
        expectedResult = null;
        let result = await operartion.getMatchPerYear("testdb", "matchestest");
        expect(result).deep.not.equals(expectedResult);
    })

    it("should not return empty array", async function () {
        expectedResult = [];
        let result = await operartion.getMatchPerYear("testdb", "matchestest");
        expect(result).deep.not.equals(expectedResult);
    })

    it("should return array of objects", async function () {
        expectedResult =
            [{ label: 2015, y: 6 },
            { label: 2016, y: 3 },
            { label: 2017, y: 4 },
            { label: 2018, y: 7 }];
        let result = await operartion.getMatchPerYear("testdb", "matchestest");
        expect(result).deep.equals(expectedResult);
    })
})


describe("unit testing extra run conceded by teams", function () {

    it("should not return empty array of object", async function () {
        expectedResult = [];
        let result = await operartion.getExtraRunsConcededByTeams("testdb", "matchestest2016", "deliveriestest2016");
        expect(result).deep.not.equals(expectedResult);
    })

    it("should return extra runs given by bowling team", async function () {
        expectedResult = 
        [{label: 'Kings XI Punjab', y: 6 },
        { label: 'Gujarat Lions', y: 7 },
        { label: 'Delhi Daredevils', y: 11 },
        { label: 'Kolkata Knight Riders', y: 12 }];
        let result = await operartion.getExtraRunsConcededByTeams("testdb", "matchestest2016", "deliveriestest2016");
        expect(result).deep.equals(expectedResult);
    })
})


describe("unit testing economy rate of bowlers", function () {
    it("should return bowlers and their economy rate", async function () {
        expectedResult = [
          {
            label: "R Ashwin",
            y: 0
          },
          {
            label: "A Nehra",
            y: 12
          },
          {
            label: "DJ Bravo",
            y: 14
          },
          {
            label: "RA Jadeja",
            y: 26
          }
        ];
        let result = await operartion.topEconomicalBowler("testdb", "matchestest2015", "deliveriestest2015");
        expect(result).deep.equals(expectedResult);
    })
})