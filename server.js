function loadChart(path) {
  let data = [];
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status == 200) {
      data = JSON.parse(this.responseText);
    }
  };
  xhttp.open("GET", path, false);
  xhttp.send();
  return data;
}