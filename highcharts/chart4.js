
function drawHighChart4() {
    let inputData = loadChart("jsonFiles/chart4.json");
    console.log(inputData);
    var chart = new CanvasJS.Chart("chartContainerFourth", {
      animationEnabled: true,
      theme: "light2", // "light1", "light2", "dark1", "dark2"
      title: {
        text: "Top ten bowlers"
      },
      axisY: {
        title: "Economy"
      },
      data: [{
        type: "column",
        showInLegend: true,
        legendMarkerColor: "grey",
        legendText: "Bowlers",
        yValueFormatString:"#,##0.#",
        dataPoints: inputData
      }]
    });
    chart.render();
  }
  
drawHighChart4();  