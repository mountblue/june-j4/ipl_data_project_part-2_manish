
function drawHighChart5() {
    let inputData = loadChart("jsonFiles/chart5.json");
    var chart = new CanvasJS.Chart("manOfTheMatch", {
      animationEnabled: true,
  
      title: {
        text: "Top ten man of the match players"
      },
      axisX: {
        interval: 1
      },
      axisY2: {
        interlacedColor: "rgba(1,77,101,.2)",
        gridColor: "rgba(1,77,101,.1)",
        title: ""
      },
      data: [{
        type: "bar",
        name: "Players",
        axisYType: "secondary",
        color: "#014D65",
        dataPoints: inputData
      }]
    });
    chart.render();
  }

  drawHighChart5();