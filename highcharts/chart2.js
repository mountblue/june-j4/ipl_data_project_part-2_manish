
function drawHighChart2() {
    let inputData = loadChart("jsonFiles/chart2.json");
    // let season = loadChart("jsonFiles/season.json");
    Highcharts.chart('barChart', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Stacked bar chart'
      },
      xAxis: {
        categories: ['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017']
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Matches Won'
        }
      },
      legend: {
        reversed: true
      },
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: inputData
    });
  }

  drawHighChart2();