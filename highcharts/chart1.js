
function drawHighChart1() {
  let inputData = loadChart("jsonFiles/chart1.json");
  console.log(inputData);
  var chart = new CanvasJS.Chart("chartContainerFirst", {
    animationEnabled: true,
    theme: "light2", 
    title: {
      text: "Total matches per year"
    },
    axisY: {
      title: "Matches"
    },
    data: [{
      type: "column",
      showInLegend: true,
      legendMarkerColor: "grey",
      legendText: "Years",
      dataPoints: inputData
    }]
  });
  chart.render();

}

drawHighChart1();  