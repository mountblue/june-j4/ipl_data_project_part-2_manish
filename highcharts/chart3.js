
function drawHighChart3() {
    let inputData = loadChart("jsonFiles/chart3.json");
    console.log(inputData);
    var chart = new CanvasJS.Chart("chartContainerThird", {
      animationEnabled: true,
      theme: "light2", 
      title: {
        text: "Extra runs conceded"
      },
      axisY: {
        title: "Extra Runs"
      },
      data: [{
        type: "column",
        showInLegend: true,
        legendMarkerColor: "grey",
        legendText: "Teams",
        dataPoints: inputData
      }]
    });
    chart.render();
  
  }
  
drawHighChart3();  