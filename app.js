const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://127.0.0.1:27017";

function getMatchPerYear(database, collection) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db(database);
                let matchCollection = matchDB.collection(collection);
                matchCollection.aggregate(
                    [{
                        $group: {
                            _id: "$season",
                            total: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $sort: {
                            _id: 1
                        }
                    },
                    {
                        $project:
                            {
                                _id: "$null",
                                "label": "$_id",
                                "y": "$total"
                            }
                    }
                    ]
                ).toArray((err, data) => {
                    if (err) {

                    } else {
                        console.log(data);
                        require("fs").writeFile("jsonFiles/chart1.json", JSON.stringify(data), (err) => {
                            if (err) {
                                throw err
                            }
                            console.log("The file has been saved!")
                        })
                        resolve(data);
                    }
                })
            }
            conn.close();
        })
    })
}
// getMatchPerYear("matchesdb", "matches")


function getMatchesWonByTeams(database, collection) {

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db(database);
                let matchCollection = matchDB.collection(collection);
                matchCollection.aggregate(
                    [{

                        $group: {
                            _id: {
                                winner: "$winner",
                                season: "$season"
                            },
                            total: {
                                $sum: 1
                            }
                        }
                    }, {
                        $project: {
                            team: "$_id.winner",
                            season: "$_id.season",
                            count: "$total",
                            _id: 0
                        }
                        //     },
                        //     { 
                        //         $group: {
                        //             _id: "$team",
                        //             won: {
                        //                 $push: "$count"
                        //             }
                        //         }
                        //     },

                        //     {
                        //         $project: {
                        //             _id: "$null",
                        //             "name": "$_id",
                        //             "data": "$won"

                        // }
                    }
                    ]
                ).toArray((err, data) => {
                    if (err) {
                        console.log(err);
                    } else {

                        let data1 = highChartFormateData(data);

                        require("fs").writeFile("jsonFiles/chart2.json", JSON.stringify(data1), (err) => {
                            if (err) {
                                throw err
                            }
                            console.log("The file has been saved!")
                        })
                        console.log(data);
                        resolve(data);
                    }
                })
            }
            conn.close();
        })
    })
}
getMatchesWonByTeams("matchesdb", "matches");

function highChartFormateData(data) {
    let winnerTeams = [];
    let teams = [];
    let seasons = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017];
    let teamObj = {};

    data.forEach(element => {

        let team = element["team"];
        let season = element["season"];
        let count = element["count"];

        if (team.length > 0) {

            if (!teams.includes(element.team)) {
                teams.push(element.team);
            }

            if (teamObj.hasOwnProperty(element.team)) {
                teamObj[team][season] = element.count;
            } else {
                teamObj[team] = {};
                for (let i = 0; i <seasons.length ; i++) {
                    teamObj[team][seasons[i]] = 0;
                }
                teamObj[team][season] = element.count
            }
        }
    });

    teams.forEach(element=>{
        winnerTeams.push({
            name : element,
            data:Object.values(teamObj[element])
        });
    })

    return winnerTeams;
}

function getExtraRunsConcededByTeams(matchDb, matchCollection, deliveriesCollection) {

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db(matchDb);
                let matchCollection1 = matchDB.collection(matchCollection);
                matchCollection1.aggregate(
                    [{
                        $match: {
                            season: 2016
                        }

                    },

                    {
                        $lookup: {
                            from: deliveriesCollection,
                            localField: "id",
                            foreignField: "match_id",
                            as: "deliveriesNewDB"
                        }
                    },
                    {
                        $unwind: "$deliveriesNewDB"
                    },

                    {
                        $project: {
                            _id: 0,
                            "deliveriesNewDB": {
                                "extra_runs": 1,
                                "bowling_team": 1
                            }
                        }
                    },
                    {
                        $group: {
                            _id: "$deliveriesNewDB.bowling_team",
                            total: {
                                $sum: "$deliveriesNewDB.extra_runs"
                            }
                        }
                    },
                    {
                        $sort: {
                            total: 1
                        }
                    },
                    {
                        $project:
                            {
                                _id: "$null",
                                "label": "$_id",
                                "y": "$total"
                            }
                    }]
                ).toArray((err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        require("fs").writeFile("jsonFiles/chart3.json", JSON.stringify(data), (err) => {
                            if (err) {
                                throw err
                            }
                            console.log("The file has been saved!")
                        })
                        resolve(data);
                    }
                })
            }
            // conn.close();
        })
    })
}
// getExtraRunsConcededByTeams("matchesdb", "matches", "deliveries")



function topEconomicalBowler(matchDb, matchCollection, deliveriesCollection) {

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db(matchDb);
                let matchCollection1 = matchDB.collection(matchCollection);
                matchCollection1.aggregate(
                    [{
                        $match: {
                            season: 2015
                        }
                    },
                    {
                        $lookup: {
                            from: deliveriesCollection,
                            localField: "id",
                            foreignField: "match_id",
                            as: "deliveriesNewDB"
                        }
                    },
                    {
                        $unwind: "$deliveriesNewDB"
                    },

                    {
                        $project: {
                            _id: 0,
                            "deliveriesNewDB": {
                                "total_runs": 1,
                                "bowler": 1,
                                "ball": 1,
                                "noball_runs": 1,
                                "wide_runs": 1
                            }
                        }
                    },

                    {
                        $group: {
                            _id: "$deliveriesNewDB.bowler",
                            totalRun: {
                                $sum: "$deliveriesNewDB.total_runs"
                            },

                            totalBall: {
                                $sum: {
                                    $cond: {
                                        if: {
                                            $gt: ["$deliveriesNewDB.noball_runs", 0]
                                        },
                                        then: 0,
                                        else: {
                                            $cond: {
                                                if: {
                                                    $gt: ["$deliveriesNewDB.wide_runs", 0]
                                                },
                                                then: 0,
                                                else: 1
                                            }
                                        }
                                    }
                                },

                            }
                        },

                    },

                    {
                        $project: {
                            _id: 1,
                            economy: {
                                $multiply: [{
                                    $divide: ["$totalRun", "$totalBall"]
                                }, 6]
                            }
                        }
                    },
                    {
                        $sort: {
                            economy: 1
                        }
                    }, {
                        $limit: 10
                    },
                    {
                        $project:
                            {
                                _id: "$null",
                                "label": "$_id",
                                "y": "$economy"
                            }
                    }
                    ]
                ).toArray((err, data) => {
                    if (err) {
                        console.log(err);
                    } else {

                        require("fs").writeFile("jsonFiles/chart4.json", JSON.stringify(data), (err) => {
                            if (err) {
                                throw err
                            }
                            console.log("The file has been saved!")
                        })
                        resolve(data);
                    }
                })
            }
            // conn.close();
        })
    })
}
// topEconomicalBowler("matchesdb", "matches", "deliveries");



function topManOfTheMatchPlayer(matchDb, matchCollection) {

    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (error, conn) {
            if (error) {
                reject(error.code);
            } else {
                let matchDB = conn.db(matchDb);
                let matchCollection1 = matchDB.collection(matchCollection);
                matchCollection1.aggregate(
                    [{
                        $group: {
                            _id: "$player_of_match",
                            total: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $sort: {
                            total: -1
                        }
                    },
                    {
                        $limit: 10
                    },
                    {
                        $project:
                            {
                                _id: "$null",
                                "label": "$_id",
                                "y": "$total"
                            }
                    }
                    ]
                ).toArray((err, data) => {
                    if (err) {
                        console.log(err);
                    } else {

                        require("fs").writeFile("jsonFiles/chart5.json", JSON.stringify(data), (err) => {
                            if (err) {
                                throw err
                            }
                            console.log("The file has been saved!")
                        })
                        resolve(data);
                    }
                })
            }
            // conn.close();
        })
    })
}
// topManOfTheMatchPlayer("matchesdb", "matches");

module.exports = {
    getMatchPerYear: getMatchPerYear,
    getMatchesWonByTeams: getMatchesWonByTeams,
    getExtraRunsConcededByTeams: getExtraRunsConcededByTeams,
    topEconomicalBowler: topEconomicalBowler,
    topManOfTheMatchPlayer: topManOfTheMatchPlayer
}